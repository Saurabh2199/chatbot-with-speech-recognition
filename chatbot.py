from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
import random
import speech_recognition as sr 
import pyttsx3  

stopwords = stopwords.words('english')
port = PorterStemmer()
father = {
    ('countri',2):['United Kingdom !','Hes country is United Kingdom','He is from Uk'],
    ('name',1):['My Father name is Ajit Patil',' Ajit Patil!','Hes name is Ajit Patil'],
    ('age',1):['51 !','He is 51 years old','He is 51']

}
mother = {
    ('countri',2):['United States !','Shes country is United States','She is from US'],
    ('name',1):['My mother name is Meenakshi',' Meenakshi!','Her name is Meenakshi'],
    ('age',1):['47 !','she is 47 years old','she is 47']

}
name = {
    ('countri',2):['India !','My country is India','i am from India'],
    ('name',1):['My name is Saurabh',' Saurabh!','Myself Saurabh'],
    ('age',1):['21 !','i am 21 years old','i am currently 21']

}

priority = {'father':2,'mother':2,'your':1}
sent = "who is your father"
response = ''


  
r = sr.Recognizer() 
#output command
def SpeakText(command): 
    engine = pyttsx3.init() 
    engine.say(command)  
    engine.runAndWait() 
      
def choose_response(direct,data):
    prio = 0
    count = 0
    for i in direct.keys():
        if i[0] in data and i[1] > prio:
            prio = i[1]
            count += 1
            response = random.choice(direct.get(i))
            
    if(count == 0):
        response =  random.choice(['My name is Saurabh',' Saurabh!','Myself Saurabh'])
        
    return response               
            
            
def assignning_directory(clean,char):
    if char =='father': response = choose_response(father,clean)
    elif char=='mother':response = choose_response(mother,clean) 
    elif char =='your':response = choose_response(name,clean)
    else: response =random.choice(['I dont know Sorry !','I dont have that knowledge', 'I am not told about that'])
    
    return response
#chooses the character
def choose_char(clean):
    val = 0
    char = ''
    for i in range(len(clean)):
        clean[i] = port.stem(clean[i])
        if(clean[i] in priority.keys() and priority.get(clean[i]) >val):
            val = priority.get(clean[i])
            char = clean[i]
    response = assignning_directory(clean,char)
    
    return response
    
    #splitting and removing stopwords
def cleaning(data):
    sent = data.split(' ')
    clean = []
    for i in sent:
        if i == 'your':
            clean.append(i)
        if i not in stopwords:
            clean.append(i)
    response = choose_char(clean)           #calling the character recognizer
    return response

while(1):      
    try: 
        with sr.Microphone() as source2:  
            r.adjust_for_ambient_noise(source2, duration=0.2)   
            audio2 = r.listen(source2)  
            MyText = r.recognize_google(audio2) 
            MyText = MyText.lower() 
            print("YOU:",MyText)
            say = cleaning(MyText)
            print("BOT:",say)
            SpeakText(say)
              
    except sr.RequestError as e: 
        print("Could not request results; {0}".format(e)) 
          
    except sr.UnknownValueError: 
        print("unknown error occured") 
        
        
        
        
        
        
        
        
        